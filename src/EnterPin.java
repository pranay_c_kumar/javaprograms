import java.util.Scanner;
public class EnterPin {

	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		System.out.println("Welcome to DIB");
		System.out.println();
		System.out.println("Please enter your pin:");
		int pin = s.nextInt();
		int pinNo = 1615;
		int tries = 1;
		
		while(pin != pinNo && tries < 3) {
			System.out.println("Incorrect Pin. Please try again");
			System.out.println();
			System.out.println("Enter your Pin");
			pin = s.nextInt();
			tries++;
		}
		if(pin == pinNo) {
			System.out.println("Pin Accepted. May I help you?");
		}else {
			System.out.println("Your Account is blocked");
		}
		
	}
}
