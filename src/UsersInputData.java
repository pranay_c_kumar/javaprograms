import java.util.Scanner;
	
public class UsersInputData {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
	System.out.println("Please enter the following information so I can sell it for a profit!");
	System.out.print("First Name:");
	String Firstname = s.next();
	System.out.print("Last Name:");
	String Lastname = s.next();
	System.out.print("Grade (9-12):");
	int Grade = s.nextInt();
	System.out.print("StudentID:");
	long StudentID = s.nextLong();
	System.out.print("Login:");
	String Login = s.next();
	System.out.print("GPA (0.0-4.0):");
	double GPA = s.nextDouble();
	System.out.println();
    System.out.println();
    System.out.println("Your information:");
    System.out.println("\tLogin: " + Login);
    System.out.println("\tID:    " + StudentID);
    System.out.println("\tName:  " + Lastname + ", " + Firstname);
    System.out.println("\tGPA:   " + GPA);
    System.out.println("\tGrade: " + Grade);
	
	
	}
}
