import java.util.Scanner;

public class HowOldAreYou {
	public static void main (String[] args) {
		Scanner s = new Scanner(System.in);
		String name;
		int Age;
		System.out.println("Hey, What is your name?");
		name = s.next();
		System.out.println("Ok, " + name + ", how old are you? ");
		Age = s.nextInt();
		System.out.println();
        if (Age < 16) {
            System.out.println("You can't drive, " + name + ".");
        }
        if (Age < 18) {
            System.out.println("You can't vote, " + name + ".");
        }
        if (Age < 25) {
            System.out.println("You can't rent a car, " + name + ".");
        }
        if (Age >= 25) {
            System.out.println("You can do anything that's legal, " + name + ".");
        }
		
	}

}
