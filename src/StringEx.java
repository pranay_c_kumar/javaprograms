
public class StringEx {
	// 1. String Literal
	// 2. String Object
	
public static void main(String[] args) {
	

	String str1 = "Operator"; //String Literal
	char chr[] = {'O','p','e','r','a','t','o','r'};
	
	String str2 = new String ("Operator"); //String with new keyword
	
	System.out.println(str1);
	System.out.println(str2);
	
}
	
}
