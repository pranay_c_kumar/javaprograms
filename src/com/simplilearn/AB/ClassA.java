package com.simplilearn.AB;

import com.simplilearn.A.DataAccess;

public class ClassA {
	
	private static int privateV =100;
	int defaultV;
	protected int protectedV;
	public int publicV =100;
	
	
	private class PrivateClass{}
	class DefaultClass{}
	protected class ProtectedClass{}
	public class PublicClass{}
	
	private void privateMethod() {};
	void defaultMethod() {};
	protected int protectedMethod() { return 10; }
	public void publicMethod() {};
	
	public static void main (String[] args) {
		ClassA d = new ClassA();
	}
}
