package com.simplilearn.A;

public class DataAccess {
	
	// private is accessible with in a class
	private static int privateV =100;
	// default is accessible with in a package
	int defaultV;
	// protected is accessible with in a class, package, outside the package but with inheritance.
	protected int protectedV;
	// public is accessible anywhere
	public int publicV =100;
	
	
	//inner class
	private class PrivateClass{}
	class DefaultClass{}
	protected class ProtectedClass{}
	public class PublicClass{}
	
	private void privateMethod() {};
	void defaultMethod() {};
	protected int protectedMethod() { return 10; }
	public void publicMethod() {};
	
	public static void main (String[] args) {
		DataAccess d = new DataAccess();
		
	}

}
