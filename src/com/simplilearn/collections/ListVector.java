package com.simplilearn.collections;
import java.util.Enumeration;
//import java.util.Iterator;
import java.util.Vector;

public class ListVector {
	
	public static void main(String[] args) {
		Vector<String> hero = new Vector <String> ();
		
		hero.add("Superman");
		hero.add("Heman");
		hero.add("Ironman");
		hero.addElement("Batman");
		for (int i = 0; i<7; i++)
			hero.addElement("hero"+i);
		
		System.out.println(hero.capacity());
		System.out.println(hero);
		
//		Iterator<String> itr = hero.iterator();
//		while(itr.hasNext()) {
//		System.out.println(itr.next());
//		}
		// Using Enumerator 
		Enumeration <String> enumr = hero.elements();
		while(enumr.hasMoreElements()) {
			System.out.println(enumr.nextElement());
		}
	}
	
}
