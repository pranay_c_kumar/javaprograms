package com.simplilearn.collections;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class EmpCollections {

	public static void main(String[] args) {
		
		Employee e1 = new Employee(101, "Rajiv", 52000.00f, "Dev");
		Employee e2 = new Employee(101, "Raj", 52000.00f, "Dev Support");
		Employee e3 = new Employee(101, "Sam", 52000.00f, "Dev Support");
		Employee e4 = new Employee(101, "Tarun", 52000.00f, "Dev Support");
		
		List<Employee> employeeList = new ArrayList <Employee> ();
		employeeList.add(e1);
		employeeList.add(e2);
		employeeList.add(e3);
		employeeList.add(1,e4);
	//	System.out.println(employeeList.get(1).empName);
		
		System.out.println("==========================");
		// Iterating a collection with Iterator.
		Iterator<Employee> empIterator = employeeList.iterator();
		while (empIterator.hasNext()) {
			System.out.println(empIterator.next().empName);
			
		}
		System.out.println("==========================");
		// Iterating a collection with For Loop.
		for (int i = 0;  i <employeeList.size(); i++) {
			System.out.println(employeeList.get(i).empName);
		}
		System.out.println("==========================");
		// Iterating a collection with For-each Loop with colon.
		for (Employee emp : employeeList) {
			System.out.println(emp.empName + " " + emp.empDept);
		}
		System.out.println("==========================");
		// Iterating a collection with For-each Loop -> Syntax with function
		employeeList.forEach((emp)->{
			System.out.println(emp.empName + " " + emp.empDept);
		});
	}
}
