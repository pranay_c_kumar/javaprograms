package com.simplilearn.collections;

public class Employee {

	int empNo;
	String empName;
	float empSal;
	String empDept;
	
	Employee(int empNo,String empName,float empSal,String empDept){
		this.empNo = empNo;
		this.empDept = empDept;
		this.empName = empName;
		this.empSal = empSal;		
	}
}
