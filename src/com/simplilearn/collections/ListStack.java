package com.simplilearn.collections;

import java.util.Enumeration;
import java.util.Stack;

public class ListStack {
	
	public static void main(String[] args) {
		
		Stack<String>hero = new Stack<String>();
		
		hero.push("Superman");
		hero.push("Heman");
		hero.push("Ironman");
		hero.addElement("Batman");
		hero.pop();
	//	for (int i = 0; i<7; i++)
	//		hero.addElement("hero"+i);
		
		System.out.println(hero.capacity());
		System.out.println(hero);
		System.out.println(hero.peek());
		System.out.println(hero.search("Heman"));
		
		Enumeration <String> enumr = hero.elements();
		while(enumr.hasMoreElements()) {
			System.out.println(enumr.nextElement());
	}

}
}