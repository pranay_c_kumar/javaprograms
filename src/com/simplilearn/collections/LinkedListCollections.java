package com.simplilearn.collections;

import java.util.Iterator;
import java.util.LinkedList;

public class LinkedListCollections {
	
	public static void main(String[] args) {
		LinkedList<String> empNames = new LinkedList<String>();
		empNames.add("Raj");
		empNames.add("Tarun");
		empNames.add("Vicky");
		empNames.set(0, "Jack");
		empNames.add(2, "Ryan");
		empNames.addFirst("Major");
		empNames.addLast("Killer");
		//empNames.clear();
		//empNames.isEmpty();
		System.out.println(empNames);
		
		Iterator<String> Namesitr = empNames.iterator();
		while(Namesitr.hasNext()) {
		System.out.println(Namesitr.next());
		}
	}

}
