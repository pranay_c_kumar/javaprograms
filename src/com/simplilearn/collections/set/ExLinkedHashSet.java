package com.simplilearn.collections.set;

//import java.util.HashSet;
import java.util.Iterator;
//import java.util.HashSet;
import java.util.LinkedHashSet;

public class ExLinkedHashSet {

	public static void main(String[] args) {
		
		LinkedHashSet<Integer> uniqueDigits = new LinkedHashSet<Integer>();
		uniqueDigits.add(10);
		uniqueDigits.add(110);
		uniqueDigits.add(180);
		uniqueDigits.add(10);
		uniqueDigits.add(13);
		System.out.println(uniqueDigits);
		
		Iterator<Integer> itr = uniqueDigits.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
		
		//HashSet and LinkedHashSet doesn't allows duplicate values. Insertion order is preserved in LinkedHashSet.

	}

}
