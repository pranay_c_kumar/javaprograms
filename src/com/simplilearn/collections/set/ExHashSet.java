package com.simplilearn.collections.set;

import java.util.HashSet;

public class ExHashSet {

	public static void main(String[] args) {
		HashSet<Integer> uniqueDigits = new HashSet<Integer>();
		uniqueDigits.add(10);
		uniqueDigits.add(110);
		uniqueDigits.add(180);
		uniqueDigits.add(10);
		uniqueDigits.add(13);
		System.out.println(uniqueDigits);
		
		//Remove duplicate elements from an Array.
		HashSet<Integer> unique = new HashSet<Integer>();
		int [] arr = new int [] {1,2,3,4,6,7,7,30,5};
		System.out.println(arr);
		

		for (int i = 0; i<arr.length; i++)
			unique.add(arr[i]);
		System.out.println(unique);
	}

}
