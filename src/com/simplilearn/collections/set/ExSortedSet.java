package com.simplilearn.collections.set;

import java.util.Iterator;
//import java.util.LinkedHashSet;
import java.util.TreeSet;

public class ExSortedSet {

	public static void main(String[] args) {
		
		TreeSet<Integer> uniqueDigits = new TreeSet<Integer>();
		uniqueDigits.add(10);
		uniqueDigits.add(110);
		uniqueDigits.add(180);
		uniqueDigits.add(10);
		uniqueDigits.add(13);
		System.out.println(uniqueDigits.descendingSet());
		
		Iterator<Integer> itr = uniqueDigits.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}

		TreeSet<String> uniqueStrings = new TreeSet<String>();
		uniqueStrings.add("Ravi");
		uniqueStrings.add("Raj");
		uniqueStrings.add("Sam");
		System.out.println(uniqueStrings.descendingSet());
		
		Iterator<String> itr1 = uniqueStrings.iterator();
		while(itr1.hasNext()) {
			System.out.println(itr1.next());
		}
		
	}
}
