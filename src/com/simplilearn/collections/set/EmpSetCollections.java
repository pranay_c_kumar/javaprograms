package com.simplilearn.collections.set;

//import java.util.List;
import java.util.Set;
//import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class EmpSetCollections {

	public static void main(String[] args) {
		
		Employee e1 = new Employee(101, "Rajiv", 52000.00f, "Dev");
		Employee e2 = new Employee(101, "Raj", 52000.00f, "Dev Support");
		Employee e3 = new Employee(101, "Sam", 52000.00f, "Dev Support");
		Employee e4 = new Employee(101, "Tarun", 52000.00f, "Dev Support");
		
		Set<Employee> employeeSet= new HashSet <Employee> ();
		employeeSet.add(e1);
		employeeSet.add(e2);
		employeeSet.add(e3);
		employeeSet.add(e4);
	//	System.out.println(employeeList.get(1).empName);
		
		System.out.println("==========================");
		// Iterating a collection with Iterator.
		Iterator<Employee> empIterator = employeeSet.iterator();
		while (empIterator.hasNext()) {
			System.out.println(empIterator.next());
			
			
			}
			System.out.println("==========================");
			// Iterating a collection with For-each Loop with colon.
			for (Employee emp : employeeSet) {
				System.out.println(emp.empName + " " + emp.empDept);
			}
			System.out.println("==========================");
			// Iterating a collection with For-each Loop -> Syntax with function
			employeeSet.forEach((emp)->{
				System.out.println(emp.empName + " " + emp.empDept);
			});
		
			
		}
	}

