package com.simplilearn.collections;

import java.util.ArrayList;
import java.util.Iterator;

public class ListCollections {

	public static void main(String[] args) {
		
		//ArrayList <datatype> collection_name = new ArrayList <datatype> (); -> Syntax
		
		ArrayList <String> empNames = new ArrayList <String> ();
		// adding elements. 		
		empNames.add("Malik");
		empNames.add("Raj");
		empNames.add("Ramu");
		empNames.add("Malik");
		System.out.println(empNames);
		//add element based on index note : remember shifting of element
		empNames.add(2, "Pranay");
		//replacing element
		empNames.set(2, "Major");
		System.out.println(empNames);
		System.out.println(empNames.get(2));
		System.out.println(empNames.contains("Major"));
		System.out.println(empNames.indexOf("Major"));
		
		//Iterating ArrayList.
		Iterator<String> empIterator = empNames.iterator();
		
		// Three important methods of Iterator - hasNext(), next (), remove ().
		
		while(empIterator.hasNext()) {
			System.out.println(empIterator.next());
			
		}
		
		
	}
}
