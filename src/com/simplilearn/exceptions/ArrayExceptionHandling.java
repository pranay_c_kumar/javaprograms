package com.simplilearn.exceptions;

public class ArrayExceptionHandling {
	
	public static void main(String[] args) {
		
		try {
		int intArray[] = new int [] {1,2,3,4,5};
		//or
		int [] intArray2;
		
		//byte byteArray[];
		//short shortArray[];
		//String stringArray[] = new String [] {"Hello", "Hi", "Bye";
		//Object objectArray[];
		
		//add value in Array --> by using Index;
		//intArray [0] = 100;
		//intArray [1] = 100;
				
		for(int index = 0; index<intArray.length; index++) {
			System.out.println(intArray[index]);
		}
		}
		catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Error is handled");
		}
		
	}

}
