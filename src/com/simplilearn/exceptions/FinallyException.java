package com.simplilearn.exceptions;

public class FinallyException {
	
	public static void main(String[] args) {
		
		try {
			String s1 = "10abc";
			int z = Integer.parseInt(s1);
			System.out.println("Result:" + " " + z);
			}catch(Exception e) {
				System.out.println("Handled Exception");
			}	
		
		finally {
				System.out.println("Always Execute");
			}
	}

}
