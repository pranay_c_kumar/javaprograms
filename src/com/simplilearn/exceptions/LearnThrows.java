package com.simplilearn.exceptions;

import java.io.IOException;

public class LearnThrows {
	
	static int balance = 10000;
	public static void deposit (int amount) throws IOException {
		if (amount < 0) {
			throw new IOException ("Invalid amount in Input");
			
	}else {
		balance = balance + amount;
		System.out.println(balance);
	}
	
	}
	public static void main(String[] args) throws IOException {
		
		deposit(-1000);
	}

}
