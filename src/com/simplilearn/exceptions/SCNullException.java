package com.simplilearn.exceptions;

public class SCNullException {
	public static void main(String[] args) {
		try {
		String s1 = "10abc";
		int z = Integer.parseInt(s1);
		System.out.println(z);
	}
		catch(NullPointerException e) {
			System.out.println("Handled Null Pointer Exception");
		}
		catch(NumberFormatException e) {
			System.out.println("Handled Number Format Exception");
		}
		catch(Exception e) {
			System.out.println("Handled Exception");
		}
	}
}