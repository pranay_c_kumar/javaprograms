package com.simplilearn.exceptions;

public class FirstException {
	
	public static void main(String[] args) {
		try {
		int num1 = 100;
		int num2 = 0;
		System.out.println("First number" + " " + num1);
		int z = num1 / num2; 
		System.out.println("First number" + num1);
		System.out.println("Second number" + num2);
		System.out.println("Third number" + z);
		} 
			catch (ArithmeticException e) {
				System.out.println("Exception Occured");
				e.printStackTrace();
		}
			System.out.println("Remaining Statements");
	}

}
