package com.simplilearn.inheritance;

public class InhC extends InhB {

	public void methodC() {
		System.out.println("Sub-Sub / Child Class method");
	}
	
	public static void main(String args[]) {
		InhC c = new InhC();
		c.methodA();
		c.methodB();
		c.methodC();
		
	}
}
