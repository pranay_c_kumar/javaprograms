package com.simplilearn.inheritance;

public class InhB extends InhA {

	public void methodB() {
		System.out.println("Sub / Child Class method");
	}
	
	public static void main(String args[]) {
		InhB b = new InhB();
		//calling super / parent calls method
		b.methodA();
		//calling local method or current class method
		b.methodB();
	}
}
