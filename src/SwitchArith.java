import java.util.Scanner;

public class SwitchArith {
	
	public static void main(String[] args) {
    	char operator;
    	int number1, number2, result;
    	
    	Scanner s = new Scanner(System.in);
    	System.out.print("Enter operator (either +, -, * or /): ");
    	operator = s.next().charAt(0);
    	System.out.print("Enter number1 and number2 respectively: ");
    	number1 = s.nextInt();
    	number2 = s.nextInt();
    	
    	switch (operator) {
         case '+':
           result = number1 + number2;
    	   System.out.print(number1 + "+" + number2 + " = " + result);
           break;
         case '-':
           result = number1 - number2;
           System.out.print(number1 + "-" + number2 + " = " + result);
           break;
         case '*':
           result = number1 * number2;
           System.out.print(number1 + "*" + number2 + " = " + result);
           break;
         case '/':
           result = number1 / number2;
           System.out.print(number1 + "/" + number2 + " = " + result);
           break;
         default: 
           System.out.println("Invalid operator!");
           break;
        }       
    }
}


