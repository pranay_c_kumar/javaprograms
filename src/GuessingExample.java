import java.util.Scanner;

public class GuessingExample {
	public static void main(String[] args) {
		Scanner s =  new Scanner (System.in);
		System.out.println("Guess a number between 1 and 10");
		int n = s.nextInt();
		int lotNo = 5;
		do {
			System.out.println("Try again number between 1 and 10");
			n =s.nextInt();
		}while(n!=lotNo);
		System.out.println("Congratulations your guess is correct");
	}
}
