import java.util.Scanner;

public class ForLoop {
	
	public static void main(String[] args) {
		
		//for(initialization; condition; inc/dec);{}
		//for(int n=1; n<5; n++) {
			//System.out.println("The number :"+ n);
		
		//for(int vowsCount =1; vowsCount !=7; vowsCount++) {
			//System.out.println("Taking a Vows"+ vowsCount);
		
		Scanner s = new Scanner(System.in);
		System.out.println("Welcome to DIB");
		System.out.println();
		System.out.println("Please enter your pin:");
		
		int pinNo = 1615;
		int tries = 2;
		
		for(int pin = s.nextInt();pin != pinNo;) {
			System.out.println("Incorrect Pin. Please try again");
			System.out.println("Enter your Pin");
			pin = s.nextInt();
			tries++;
			if(pin == pinNo) {
				System.out.println("Pin Accepted. May I help you?");
			}else {
				System.out.println("Your Account is blocked");
			}
		
		}
}
}