
public class ObjectTypeConversion {
	public static void main(String[] args) {
		
		//Converting an Primitive to Object
		
		int number = 100;
		int number2 = 102;
		Integer obj = Integer.valueOf(number); //Converting int to Integer explicitly
		Integer objNum = number2;  //Implicit Type -> Auto boxing
		System.out.println(number + " " + obj);
		System.out.println(number2 + " " + objNum);
		
		//Converting Object type to Primitive
		
		Integer obj2 = new Integer(100);
		Integer Obj2 = new Integer(500);
		int num = obj2.intValue(); //Converting object int into primitive int by Unboxing (Explicitly)
		int num6 = Obj2;
		System.out.println(num + " " + obj2);
		System.out.println(num6 + " " + obj2);
		
	    float number1 = 200;
		Float obj1 = Float.valueOf(number1);
		System.out.println(number1 + " " + obj1);
		
		double number3 = 200d;
		Double obj3 = Double.valueOf(number3);
		System.out.println(number3 + " " + obj3);
		
		Float obj4 = new Float(200);
		float num1 = obj4.floatValue();
		System.out.println(num1 + " " + obj4);
		
		Double obj5 = new Double(200);
		double num2 = obj5.doubleValue();
		System.out.println(num2 + " " + obj5);
	}
}
