
public class UnaryOperator {
	public static void main(String[] args) {
		int a = 20;
		System.out.println(a); //20
		System.out.println(a++); //20
		System.out.println(++a); //22
		
		int c = a;
		System.out.println(c++); //22
		System.out.println(--c); //22
		System.out.println(++c); //23
		System.out.println(c++); //23
		System.out.println(c--); //24
		
		float z = c;
		System.out.println(z); 
		System.out.println(z + 1); 
		System.out.println(z++); 
		System.out.println(--z); 
		
		//short notation
		int w = (int)(z + z);
		System.out.println(w);
		System.out.println(z+=z); // z = z + z;
		
		
	}
}
