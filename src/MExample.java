
public class MExample {
	
	int a;
	int b;
	
	//no arg construct / Zero parameterized constructor / default constructor
	MExample (){}
	
	MExample (int x , int y){
		this.a = x;
		this.b = y;
	}

	void showdata () {
	System.out.println("A is" + this.a);
	System.out.println("B is" + this.b);	
	
	}
	static void myMethod() {
		System.out.println("Void non parameterized function");
	}
	static void yourMethod(int number) {
		System.out.println("Void paramterized function");
		System.out.println("Param" + number);
		}
	static float weMethod(int number, float fNumber) {
		System.out.println("void type 2 parameterized function");
		System.out.println("Param 1" + number);
		System.out.println("Param 2" + fNumber);
		return fNumber;}
	
	static int ourMethod() {
		System.out.println("Void non paramterized function");
		return 10;
	}
	public static void main(String[] args) {
		myMethod();
		yourMethod(12);
		weMethod(12 , 200f);
		ourMethod();
		
	}
}
