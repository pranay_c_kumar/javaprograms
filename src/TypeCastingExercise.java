import java.util.Scanner;

public class TypeCastingExercise {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.print("Age:");
		int Age = s.nextInt();
		System.out.print("Salary:");
		float Salary = s.nextFloat();
		System.out.print("AccountID:");
		long AccountID = s.nextLong();
		System.out.print("BankBalance:");
		double BankBalance = s.nextDouble();
		
		int add = Age + (int)(Salary + AccountID + BankBalance);
		int multiply = Age * (int) (Salary * AccountID * BankBalance);
		byte Divide = (byte) (BankBalance/AccountID);
		byte Subtract = (byte)(BankBalance-AccountID);
		
		System.out.println("Add:" + add);
		System.out.println("Multiply:" + multiply );
		System.out.println("Divide:" + Divide );
		System.out.println("Substract:" + Subtract);
	}

}
