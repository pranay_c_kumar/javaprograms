
public class EncapBank {
	
	public String bankName;
	private int accountNo;
	private String accountName;
	private float accBalance;
	private int phoneNumber;
	
	public int getAccountNo() {
		return this.accountNo;
	}
	public void setAccountNo(int accountNo) {
		this.accountNo = accountNo;
	}
	public String getAccountName() {
		return this.accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public float getAccBalance() {
		return this.accBalance;
	}
	public void setAccBalance(float accBalance) {
		this.accBalance = accBalance;
	}
    public int getPhoneNumber() {
    	return this.phoneNumber;
    }
    public void setPhoneNumber(int phoneNumber) {
    	this.phoneNumber = phoneNumber;
    }
}
