
public class MethodCalling {
	
	
	
	//non static function need to create an object
	static void sampleCall() {
		MExample me = new MExample();
		me.myMethod();
		MExample me2 = new MExample();
		me2.myMethod();
		MExample me3 = new MExample(12 ,23);
		me3.myMethod();
		int z = me3.a;
		me3.showdata();
		
	}
	
	//Static function can be directly called with class name.
	
	public static void main(String[] args) {
		
		MExample.yourMethod(12);
		MExample.ourMethod();
		MExample.weMethod(12 , 200f);
		sampleCall();
	}
}
