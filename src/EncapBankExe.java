
public class EncapBankExe {
	public static void main(String[] args) {
		EncapBank encap = new EncapBank();//1
		EncapBankOpr encapOpr = new EncapBankOpr(); //->4
		
		//setting property -> 2
		encap.setAccountNo(1001);
		encap.setAccountName("JOhn Smith");
		encap.setPhoneNumber(12345678);
		encap.setAccBalance(10000.0f);
			
		encap.bankName = "HDFC BANK";
		
		
		// ->3 get all properties
		System.out.println("-----------------------------------------------");
		System.out.println("Welcome to " + encap.bankName);
		System.out.println("Account Number is " + encap.getAccountNo());
		System.out.println("Account Name is " + encap.getAccountName());
		System.out.println("Your phone number is "+ encap.getPhoneNumber());
		System.out.println("Your Account Balance before Interest is "+ encap.getAccBalance());
		System.out.println("-----------------------------------------------");
		
		
		//bank Functionality ->
		// passing user in bank opr -> 5
		EncapBank resp = encapOpr.addInterest(encap);
		System.out.println("Your Account Balance after Interest is "+ resp.getAccBalance());
	}

}
