import java.util.Scanner;
public class WhileLoop {
	
	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		System.out.println("Enter how many times you want to say 'Hi");
		int n = s.nextInt();
		int i = 0;
		
		while(i<n){	
			//infinity loop
			System.out.println("Hi");
			i++;
		}
		}

}
