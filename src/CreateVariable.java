
public class CreateVariable {
	public static void main(String[] args) {
		byte b = 127;
		int x =20 , y=30, z=x+y;
		short s=10;
		long l =1000L;
		long j = x+z+b;
		float f1=30.0f, f2=10.0f, f3=f1-f2;
		double d1 =10.0d, d2 =10.0d, d3=d1*d2;
		char c ='C';
		
		System.out.println("Byte Value is "+b +" Take 1 byte");
		System.out.println("integer Value is "+z +" Take 4 byte");
		System.out.println("short Value is "+s +" Take 8 byte");
		System.out.println("long Value is "+j +" Take 8 byte");
		System.out.println("float Value is "+f3 +" Take 8 byte");
		System.out.println("double Value is "+d3 +" Take 8 byte");
		System.out.println("char Value is "+c +" Take 2 byte");
	}
}
