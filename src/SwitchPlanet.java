import java.util.Scanner;

public class SwitchPlanet {
	public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int earthWeight, planet;
        double planetWeight = 0.0;
        System.out.print("Please enter your current earth weight: ");
        earthWeight = s.nextInt();
        System.out.println("I have information for the following planets:");
        System.out.println("   1. Venus   2. Mars    3. Jupiter");
        System.out.println("   4. Saturn  5. Uranus  6. Neptune");
        System.out.print("Which planet are you visiting? ");
        planet = s.nextInt();
        System.out.println();

	switch(planet) {
	case 1: planetWeight = earthWeight * 0.78; 
	System.out.println("Your weight would be " + planetWeight + " pounds on that planet.");
    break;
	case 2: planetWeight = earthWeight * 0.39;  
	System.out.println("Your weight would be " + planetWeight + " pounds on that planet.");
    break;
	case 3: planetWeight = earthWeight * 2.65;
	System.out.println("Your weight would be " + planetWeight + " pounds on that planet.");
	break;
	case 4: planetWeight = earthWeight * 1.17;
	System.out.println("Your weight would be " + planetWeight + " pounds on that planet.");
	break;
	case 5: planetWeight = earthWeight * 1.05;
	System.out.println("Your weight would be " + planetWeight + " pounds on that planet.");
	break;
	case 6: planetWeight = earthWeight * 1.23;
	System.out.println("Your weight would be " + planetWeight + " pounds on that planet.");
	break;
	default :
		System.out.println("Not a valid planet.");
		break;
	}
	}
}
