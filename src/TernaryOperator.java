
public class TernaryOperator {
	
	public static void main(String[] args) {
		int age = 21;
		int z = 0;
		if (age>20) {
			z = age;
		} else {
			z = 0;
		}			
	
	//Ternary Operator
		
		z = (age>20) ? age : (age>18)? 18 : 0;
		System.out.println(z);
		String loginMsg = (z!=0) ? "Login Success" : "Login Failed";
		System.out.println(loginMsg);
	}
	}
	
	

