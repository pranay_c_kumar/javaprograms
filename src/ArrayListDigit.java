import java.util.ArrayList;
import java.util.Iterator;


public class ArrayListDigit {

	public static void main(String[] args) {
		ArrayList <Integer> Digits = new ArrayList <Integer> ();
		Digits.add(101);
		Digits.add(102);
		Digits.add(103);
		Digits.add(104);
		Digits.add(105);
		Digits.add(106);
		Digits.add(107);
		Digits.add(108);
		Digits.add(109);
		Digits.add(110);
		System.out.println(Digits);
		Digits.set(5, 200);
		Digits.add(6,300);
		Digits.remove(7);
		
		//ArrayList <Integer> digits = new ArrayList <Integer> (Arrays.asList(101,102,103,104,105)); -> Another way of adding digits in one line. 
		//ArrayList <Integer> digits2= new ArrayList <Integer> (Arrays.asList(106,107,108,109,110));
		//digits.addAll(digits2);
		
		Iterator<Integer> DigIterator = Digits.iterator();
		while(DigIterator.hasNext()) {
			System.out.println(DigIterator.next());
		}
	}
}
